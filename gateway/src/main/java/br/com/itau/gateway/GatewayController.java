package br.com.itau.gateway;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
public class GatewayController {
	
	@Autowired
	FilmeClient filmeClient;

	@GetMapping
	public String buscarFilme() {
		return filmeClient.buscarFilme();
	}
}
